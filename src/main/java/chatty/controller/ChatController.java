package chatty.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import chatty.entity.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ChatController {

	@MessageMapping("/chat/{topic}")
	@SendTo("/topic/messages")
	public Message send(@DestinationVariable("topic") String topic, Message message) throws Exception {
		log.info("Get message on server from: {}", message.getFrom());
		return message;
	}
}
