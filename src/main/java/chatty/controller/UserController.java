package chatty.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import chatty.entity.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

	public List<User> users = new ArrayList<User>();

	@GetMapping
	public List<User> getUsers() {
		return users;
	}

	@PostMapping
	public void addUser(@RequestBody User user) {
		users.add(user);
		log.info("Actual users: {}", users.toString());
	}

	@DeleteMapping
	public void deleteUser(@RequestBody User user) {
		if (users.size() != 0) {
			users.remove(user);
		}
		log.info("Actual users: {}", users.toString());
	}

}
