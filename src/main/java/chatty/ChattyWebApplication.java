package chatty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChattyWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(ChattyWebApplication.class, args);
	}
}
