#!/bin/bash

GREEN="\x1B[32m"
LIGHT_GREEN="\x1B[92m"
NORMAL="\x1B[0m"
YELLOW="\x1B[33m"
UNDERLINED="\x1B[4m"
ARROWS="\x1B[36m>>\x1B[0m"
LIGHT_RED="\x1B[91m"
DONE_TEXT="${LIGHT_GREEN}DONE${NORMAL}"

function show_help() {
    echo    "Usage: $0 [OPTION-1] [OPTION-2] ..."
    echo
    echo    "Options:"
    echo    -e "\t-h, --help        	- show help"
    echo    -e "\t--docker		- run project as Docker"
    echo    -e "\t--localy	        - run project as Jar localy"
    echo    -e "\t--first-localy      	- ${LIGHT_RED}If you start project at firs than you need set add work host in /etc/hosts${NORMAL}"
    echo
    echo  -e "With no options provided, the script will try to build & run"
    
    echo -e "${YELLOW}INFO:${NORMAL} Notice that you should have the following"
    echo    "directory tree otherwise the script won't work:"
    echo
    echo -e "   chatty-server"
    echo -e "   ├── startup.sh"	
    echo -e "   ├── docker-compose.yaml"
    echo -e "   chatty-client"

    exit 0
}

while [[ $# -gt 0 ]]
do
    key="$1"
    
    case $key in
        -h|--help)
            show_help
        ;;
        --docker)
            cp docker-compose.yaml ../
	    ./gradlew build
	    cd ../chatty-client
	    ./gradlew build
	    cd ..
	    docker-compose up --build
            shift
        ;;
        --localy)
            ./gradlew build
	    cd ../chatty-client
	    ./gradlew build
	    cd ..
	    nohup java -jar ./chatty-server/build/libs/chatty.jar &
	    java -jar ./chatty-client/build/libs/chatty-client.jar
            shift
        ;;
        --first-localy)
            ./gradlew build
	    cd ../chatty-client
	    ./gradlew build
	    cd ..
	    sudo sed -i '' -e '$a\127.0.0.1      chatty-server-service' /etc/hosts
	    nohup java -jar ./chatty-server/build/libs/chatty.jar &
	    java -jar ./chatty-client/build/libs/chatty-client.jar &
            shift
        ;;
        *)
            echo -e "${LIGHT_RED}Error: Unknown option '$1'${NORMAL}"
            exit 1
        ;;
    esac
done
