FROM openjdk:11-oracle
EXPOSE 8383
COPY build/libs/*.jar chatty.jar
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","chatty.jar"]