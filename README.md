# chatty

As Anton Pavlovich Chekhov said: "Brevity is the sister of talent", that's why my project has the following documentation.

## Intro
The project was written using the Spring framework and WebSockets technology as a message broker in which Stomp acts. The application consists of a server and a client (both are written in Java 11).

## API
The application was written as an API, under which in the future it will be possible to impose a frontend and use it, therefore we have two stages for sending a message: **login** and **sending**. All messages are displayed in the console as logs.

**LOGIN** is POST request on `http://localhost:8384/login` with next Json body:

```
{
    "name":"John"
}
```

**SENDING** is POST request on `http://localhost:8384/` with next Json body:

```
{
    "from":"John",
    "text":"hi Ted!",
    "to":"Ted"
}
```

### For developers:
The development environment is Eclipse. To run the application, I advise you to enter `./startup.sh --help` and familiarize yourself with the possible launch options. If you want to run 2 instances of the application, then you need to change the port in the `application.property` file
To test your application with one client, you just need to send a message with the same sender and recipient:

```
{
    "from":"John",
    "text":"hi John!",
    "to":"John"
}
```
**Warning: If you first ran the project locally one or more times, then before starting in docker mode, manually delete the last added host in the file `/etc/hosts` (I could not automate this) and also delete the process running in nohup mode:**

```
sudo gedit /etc/hosts -> $(Remove last line)
sudo lsof -i :8383
sudo kill -9 $(ID)
```

